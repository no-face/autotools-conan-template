import os
from os import path

from conans import ConanFile, tools

class Recipe(ConanFile):
    name        = "libname"
    version     = "0.0.0"
    description = "lorem ipsum dolor sit amet"
    license     = "LICENSE type"
    url         = "..."  #recipe repo url

    settings    = "os", "compiler", "arch"
    options = {
        "shared" : [True, False],
    }
    default_options = (
        "shared=True",
    )

    requires    = (
    )

    build_requires    = (
        "AutotoolsHelper/0.0.2@noface/experimental"
    )

    BASE_URL_DOWNLOAD       = "..."
    FILE_URL                = "{}/...".format(BASE_URL_DOWNLOAD)
    EXTRACTED_FOLDER_NAME   = "..."
    FILE_SHA256             = "..."

    def source(self):
        zip_name = self.name + ".tar.xz"
        tools.download(self.FILE_URL, zip_name)
        tools.check_sha256(zip_name, self.FILE_SHA256)

    def build(self):
        env_vars = {}

        with tools.environment_append(env_vars):
            self.prepare_build()
            self.configure_and_make()

    def package(self):
        #Files installed in build step
        pass

    def package_info(self):
        includes = [
        ]

        self.cpp_info.includedirs   = includes
        #self.cpp_info.bindirs       = ["bin"]
        self.cpp_info.libs          = ["..."]
    
    ##################################################################################################
    
    def prepare_build(self):
        self.output.info("preparing build")

        #...
    
    def configure_and_make(self):
        with tools.chdir(self.EXTRACTED_FOLDER_NAME), tools.pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
                shared      = self.options.shared)

            #if self.options.use_example != "default":
            #    autot.with_feature("example", self.options.use_example)

            autot.configure()
            autot.build()
            autot.install()
