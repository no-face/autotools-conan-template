# **lib**-conan

[Conan.io](https://www.conan.io/) package for [**lib**][lib_link].

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
**lib** [reference manual][lib_docs] to instruction in how to use the library.

[lib_link]: http://example.com
[lib_docs]: http://example.com

## About **lib**

    Lorem ipsum dolor sit amet


## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms (see LICENSE.md).

**lib** is released under the **example license**.
The license file and source code are available at [repository][source_code].

See the **LICENSE** file [here][license_link].

[source_code]: http://example.com
[license_link]: http://example.com

## Limitations

     Lorem ipsum dolor sit amet
